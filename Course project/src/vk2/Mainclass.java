/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vk2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author Ilari Sahi
 * Opiskelijanumero: 0438594
 * 23.9.2016
 */
public class Mainclass {
    public static void main(String[] args) {      
        String tmp = null;
        int empty = 1;
        
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        
        System.out.print("Anna koiralle nimi: ");
        try {
            tmp = br.readLine();
        } catch (IOException ex) {
            System.out.println("Virhe!");
        }
        
        Dog dogi = new Dog(tmp);
        
        do {
            System.out.print("Mitä koira sanoo: ");
            try {
                tmp = br.readLine();
            } catch (IOException ex) {
                System.out.println("Virhe!");
            }
            
            empty = dogi.speak(tmp);
        } while (empty == 1);        
    }
}
