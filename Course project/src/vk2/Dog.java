/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vk2;

import java.util.Scanner;

/**
 *
 * @author Ilari Sahi
 * Opiskelijanumero: 0438594
 * 23.9.2016
 */
public class Dog {
    private String name;
    private String says;
    
    public Dog(String inputNimi) {
        inputNimi = inputNimi.trim();
        
        if (inputNimi.isEmpty()) {
            name = "Doge";
        } else {
            name = inputNimi;
        }
        
        says = "Much wow!";
        System.out.println("Hei, nimeni on " + name);
    }
    
    public int speak(String say) {
        say = say.trim();
        if (say.isEmpty()) {
            System.out.println(name + ": " + says);
            return 1;
        } else {
            Scanner scan = new Scanner(say);
            while (scan.hasNext()) {
                if (scan.hasNextBoolean()) {
                    System.out.println("Such boolean: " + scan.next());
                } else if (scan.hasNextInt()) {
                    System.out.println("Such integer: " + scan.next());
                } else {
                    System.out.println(scan.next());
                }
            }
            return 0;
        }
    }
}
