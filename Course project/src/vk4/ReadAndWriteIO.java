/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package vk4;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 *
 * @author Ilari Sahi
 * Opiskelijanumero: 0438594
 * 8.10.2016
 * 
 * Käytetyt lähteet: http://stackoverflow.com/questions/4473256/reading-text-files-in-a-zip-archive
 */
public class ReadAndWriteIO {
    private String line;
    
    public ReadAndWriteIO() {
    
    }
    
    public void read(String name) throws FileNotFoundException, IOException {
        BufferedReader br = new BufferedReader(new FileReader(name));
        while((line = br.readLine()) != null) {
            System.out.println(line);
        }
        br.close();
    }
    
    public void readAndWrite(String input, String output) throws FileNotFoundException, IOException {
        String linesep = System.getProperty("line.separator");
        String inputToOutput = "";
        BufferedReader br = new BufferedReader(new FileReader(input));
        BufferedWriter bw = new BufferedWriter(new FileWriter(output));
        
        while((line = br.readLine()) != null) {
            if(line.trim().length() < 30 && line.trim().length() > 0) {
                if (line.contains("v")) {
                    inputToOutput += line + linesep;
                }
            }
        }
        br.close();        
        bw.write(inputToOutput);
        bw.close();        
    }
    
    public void readZip(String name) throws IOException {
        ZipFile zippi = new ZipFile(name);
        Enumeration<? extends ZipEntry> entries = zippi.entries();
        ZipEntry zipEntry;
        String zipFilename;
        
        while (entries.hasMoreElements()) {
            zipEntry = entries.nextElement();
            zipFilename = zipEntry.getName();
            
            if(zipFilename.endsWith(".txt")) {
                InputStream input = zippi.getInputStream(zipEntry);
                BufferedReader br = new BufferedReader(new InputStreamReader(input, "UTF-8"));
                
                while((line = br.readLine()) != null) {
                    System.out.println(line);
                }
                input.close();
            }
        }        
        zippi.close();
    }
}
