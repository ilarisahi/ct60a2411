/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package vk4;

import java.io.IOException;

/**
 *
 * @author Ilari Sahi
 * Opiskelijanumero: 0438594
 * 8.10.2016
 */
public class Mainclass {
    public static void main(String[] args) throws IOException {
        ReadAndWriteIO io = new ReadAndWriteIO();
        
        io.readZip("zipinput.zip");

        io.read("input.txt");
    }
}
