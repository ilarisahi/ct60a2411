package vk5.vk5t5;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Ilari Sahi
 * Opiskelijanumero: 0438594
 * 5.11.2016
 */
public class Mainclass {
    public static void main(String args[]) {
        int sel;
        int wSel;
        int cSel;
        String tmp = "";
        Character character;
        character = null;
        WeaponBehavior weapon;
        weapon = null;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        do {
            System.out.println("*** TAISTELUSIMULAATTORI ***");
            System.out.println("1) Luo hahmo");
            System.out.println("2) Taistele hahmolla");
            System.out.println("0) Lopeta");

            System.out.print("Valintasi: ");
            try {
                tmp = br.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }

            sel = Integer.parseInt(tmp);

            switch (sel) {
                case 0:
                    break;
                case 1:
                    System.out.println("Valitse hahmosi:");
                    System.out.println("1) Kuningas");
                    System.out.println("2) Ritari");
                    System.out.println("3) Kuningatar");
                    System.out.println("4) Peikko");
                    System.out.print("Valintasi: ");

                    try {
                        tmp = br.readLine();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    cSel = Integer.parseInt(tmp);

                    System.out.println("Valitse aseesi:");
                    System.out.println("1) Veitsi");
                    System.out.println("2) Kirves");
                    System.out.println("3) Miekka");
                    System.out.println("4) Nuija");
                    System.out.print("Valintasi: ");

                    try {
                        tmp = br.readLine();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    wSel = Integer.parseInt(tmp);

                    switch (wSel) {
                        case 1:
                            weapon = new KnifeBehavior();
                            break;
                        case 2:
                            weapon = new AxeBehavior();
                            break;
                        case 3:
                            weapon = new SwordBehavior();
                            break;
                        case 4:
                            weapon = new ClubBehavior();
                            break;
                        default:
                            weapon = new SwordBehavior();
                            break;
                    }

                    switch (cSel) {
                        case 1:
                            character = new King(weapon);
                            break;
                        case 2:
                            character = new Knight(weapon);
                            break;
                        case 3:
                            character = new Queen(weapon);
                            break;
                        case 4:
                            character = new Troll(weapon);
                            break;
                        default:
                            character = new King(weapon);
                            break;
                    }

                    break;
                case 2:
                    if (character != null) {
                        character.fight();
                    }
                    break;

            }
        } while (sel != 0);
    }
}
