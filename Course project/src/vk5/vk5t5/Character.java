package vk5.vk5t5;

/**
 * Ilari Sahi
 * Opiskelijanumero: 0438594
 * 5.11.2016
 */
public class Character {
    protected WeaponBehavior weapon;
    protected String type;
    public void fight() {
        System.out.print(type + " ");
        weapon.UseWeapon();
    }
}

class Troll extends Character {
    public Troll(WeaponBehavior w) {
        type = "Troll";
        weapon = w;
    }
}

class King extends Character {
    public King(WeaponBehavior w) {
        type = "King";
        weapon = w;
    }
}

class Knight extends Character {
    public Knight(WeaponBehavior w) {
        type = "Knight";
        weapon = w;
    }
}

class Queen extends Character {
    public Queen(WeaponBehavior w) {
        type = "Queen";
        weapon = w;
    }
}

class WeaponBehavior {
    protected String type;

    public void UseWeapon() {
        System.out.println("tappelee aseella " + type);
    }
}

class SwordBehavior extends WeaponBehavior {
    public SwordBehavior() {
        type = "Sword";
    }
}

class ClubBehavior extends WeaponBehavior {
    public ClubBehavior() {
        type = "Club";
    }
}

class AxeBehavior extends WeaponBehavior {
    public AxeBehavior() {
        type = "Axe";
    }
}

class KnifeBehavior extends WeaponBehavior {
    public KnifeBehavior() {
        type = "Knife";
    }
}