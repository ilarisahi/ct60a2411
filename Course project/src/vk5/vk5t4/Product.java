package vk5.vk5t4;

import java.util.ArrayList;

/**
 * Ilari Sahi
 * Opiskelijanumero: 0438594
 * 5.11.2016
 */

public class Product {
    protected String manufacturer;
    protected String model;
    protected int year_of_manufacture;
    protected int weight;
}

class Car extends Product {
    private Chassis chassis;
    private Body body;
    private Engine engine;
    private ArrayList<Wheel> wheels = new ArrayList();
    private ArrayList<Door> doors = new ArrayList();
    private int i;

    public Car() {
        body = new Body();
        chassis = new Chassis();
        engine = new Engine();
        for (i=1;i<5;i++) {
            wheels.add(new Wheel());
        }
        doors.add(new Door(1));
        doors.add(new Door(2));
    }

    public void print() {
        System.out.println("Autoon kuuluu:");
        System.out.println("\tBody");
        System.out.println("\tChassis");
        System.out.println("\tEngine");
        System.out.println("\t" + wheels.size() + " Wheel");
    }
}

class Chassis extends Product {
    public Chassis() {
        System.out.println("Valmistetaan: Chassis");
        return;
    }
}

class Body extends Product {
    private String color;

    public Body() {
        color = "Green";
        System.out.println("Valmistetaan: Body");
    }
}

class Door extends Product {
    private int position;

    public Door(int x) {
        position = x;
        System.out.println("Rakennetaan ovea paikkaan " + x);
    }
}

class Engine extends Product {
    private int power;

    public Engine() {
        power = 200;
        System.out.println("Valmistetaan: Engine");
    }
}

class Wheel extends Product {
    private int diameter;

    public Wheel() {
        diameter = 18;
        System.out.println("Valmistetaan: Wheel");
    }
}
