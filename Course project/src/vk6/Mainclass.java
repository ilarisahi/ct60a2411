package vk6;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Ilari Sahi
 * Opiskelijanumero: 0438594
 * 6.11.2016
 */
public class Mainclass {
    public static void main(String args[]) {
        Bank pankki = new Bank();
        String tmp = "";
        int selection;
        String account;
        int money;
        int limit;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        do {
            System.out.println("\n*** PANKKIJÄRJESTELMÄ ***");
            System.out.println("1) Lisää tavallinen tili");
            System.out.println("2) Lisää luotollinen tili");
            System.out.println("3) Tallenna tilille rahaa");
            System.out.println("4) Nosta tililtä");
            System.out.println("5) Poista tili");
            System.out.println("6) Tulosta tili");
            System.out.println("7) Tulosta kaikki tilit");
            System.out.println("0) Lopeta");

            System.out.print("Valintasi: ");
            try {
                tmp = br.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }

            selection = Integer.parseInt(tmp);

            switch (selection) {
                case 1:
                    System.out.print("Syötä tilinumero: ");
                    try {
                        tmp = br.readLine();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    account = tmp;

                    System.out.print("Syötä rahamäärä: ");
                    try {
                        tmp = br.readLine();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    money = Integer.parseInt(tmp);

                    pankki.addAccount(account, money);

                    break;
                case 2:
                    System.out.print("Syötä tilinumero: ");
                    try {
                        tmp = br.readLine();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    account = tmp;

                    System.out.print("Syötä rahamäärä: ");
                    try {
                        tmp = br.readLine();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    money = Integer.parseInt(tmp);

                    System.out.print("Syötä luottoraja: ");
                    try {
                        tmp = br.readLine();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    limit = Integer.parseInt(tmp);

                    pankki.addAccount(account, money, limit);

                    break;
                case 3:
                    System.out.print("Syötä tilinumero: ");
                    try {
                        tmp = br.readLine();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    account = tmp;

                    System.out.print("Syötä rahamäärä: ");
                    try {
                        tmp = br.readLine();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    money = Integer.parseInt(tmp);

                    pankki.deposit(account, money);

                    break;
                case 4:
                    System.out.print("Syötä tilinumero: ");
                    try {
                        tmp = br.readLine();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    account = tmp;

                    System.out.print("Syötä rahamäärä: ");
                    try {
                        tmp = br.readLine();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    money = Integer.parseInt(tmp);

                    pankki.withdraw(account, money);

                    break;
                case 5:
                    System.out.print("Syötä poistettava tilinumero: ");
                    try {
                        tmp = br.readLine();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    account = tmp;
                    pankki.removeAccount(account);

                    break;
                case 6:
                    System.out.print("Syötä tulostettava tilinumero: ");
                    try {
                        tmp = br.readLine();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    account = tmp;
                    pankki.printAccount(account);

                    break;
                case 7:
                    System.out.println("Kaikki tilit:");
                    pankki.printAllAccounts();
                    break;
                case 0:
                    break;
                default:
                    System.out.println("Valinta ei kelpaa.");
                    break;
            }
        } while (selection != 0);
    }
}