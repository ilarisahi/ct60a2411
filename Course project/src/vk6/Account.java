package vk6;

/**
 * Ilari Sahi
 * Opiskelijanumero: 0438594
 * 6.11.2016
 */

abstract class Account {
    protected String account_number;
    protected int money;

    public Account(String n, int m) {
        account_number = n;
        money = m;
    }

    public void print() {
        System.out.println("Tilinumero: " + account_number + " Tilillä rahaa: " + money);
    }

    public void withdraw(int a) {
        if ((money-a) < 0) {
            System.out.println("Tilillä ei ole riittävästi rahaa.");
        } else {
            money -= a;
        }
    }
}

class NormalAccount extends Account{
    public NormalAccount(String n, int m) {
        super(n, m);
    }
}

class CreditAccount extends Account {
    private int credit_limit;

    public CreditAccount(String n, int m, int l) {
        super(n, m);
        credit_limit = l;
    }

    public void print() {
        System.out.println("Tilinumero: " + account_number + " Tilillä rahaa: " + money + " Luottoraja: " + credit_limit);
    }

    public void withdraw(int a) {
        if ((money + credit_limit) < a) {
            System.out.println("Tilillä ei ole riittävästi rahaa.");
        } else {
            money -= a;
        }
    }
}