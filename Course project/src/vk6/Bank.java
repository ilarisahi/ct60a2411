package vk6;

import java.util.ArrayList;

/**
 * Ilari Sahi
 * Opiskelijanumero: 0438594
 * 6.11.2016
 */
public class Bank {
    private ArrayList<Account> accounts = new ArrayList();

    private Account searchAccount(String n) {
        Account result = null;
        for (Account tmp : accounts) {
            if (tmp.account_number.equals(n)) {
                result = tmp;
                break;
            }
        }
        return result;
    }
    public void addAccount(String n, int m) {
        Account a = new NormalAccount(n, m);
        accounts.add(a);
        System.out.println("Tili luotu.");
    }
    public void addAccount(String n, int m, int l) {
        Account a = new CreditAccount(n, m, l);
        accounts.add(a);
        System.out.println("Tili luotu.");
    }
    public void removeAccount(String n) {
        Account helper = searchAccount(n);
        if (helper != null) {
            accounts.remove(helper);
            System.out.println("Tili poistettu.");
        } else {
            System.out.println("Tiliä ei löytynyt.");
        }
    }
    public void printAccount(String n) {
        Account helper = searchAccount(n);
        if (helper != null) {
            helper.print();
        } else {
            System.out.println("Tiliä ei löytynyt.");
        }
    }
    public void printAllAccounts() {
        for (Account tmp : accounts) {
            tmp.print();
        }
    }
    public void withdraw(String n, int a) {
        Account helper = searchAccount(n);
        if (helper != null) {
            helper.withdraw(a);
        } else {
            System.out.println("Tiliä ei löytynyt.");
        }
    }
    public void deposit(String n, int a) {
        Account helper = searchAccount(n);
        if (helper != null) {
            helper.money += a;
        } else {
            System.out.println("Tiliä ei löytynyt.");
        }
    }
}