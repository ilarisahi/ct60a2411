/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vk3;

/**
 *
 * @author Ilari Sahi
 * Opiskelijanumero: 0438594
 * 7.10.2016
 */
public class Bottle {
    private String name;
    private String manufacturer;
    private double total_energy;
    private double volume;
    private double price;
    
    public Bottle() {
        name = "Pepsi Max";
        manufacturer = "Pepsi";
        total_energy = 0.3;
        volume = 0.5;
        price = 1.8;
    }
    
    public Bottle(String iName, String iManufacturer, double iVolume, double iPrice) {
        name = iName;
        manufacturer = iManufacturer;
        total_energy = 0.3;
        volume = iVolume;
        price = iPrice;
    }
    
    public String name() {
        return(name);
    }
    
    public double volume() {
        return(volume);
    }
    
    public double price() {
        return(price);
    }
}
