package vk9.vk9t4t5;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

/**
 * Ilari Sahi
 * Opiskelijanumero: 0438594
 * 21.11.2016
 */
public class Movie {
    String name;
    LocalDate date;
    LocalTime startTime;
    LocalTime endTime;
    String auditorium;
    String theatre;

    @Override
    public String toString() {
       // startdate = datetimeStart.format(DateTimeFormatter.ofPattern("dd.MM.yyyy"));
       // String starttime = datetimeStart.format(DateTimeFormatter.ofPattern("HH:mm"));
       // String endtime = datetimeEnd.format(DateTimeFormatter.ofPattern("HH:mm"));

        return  date.format(DateTimeFormatter.ofPattern("dd.MM.yyyy")) + " " + name + " "
                + startTime.format(DateTimeFormatter.ofPattern("HH:mm"))
                + "-" + endTime.format(DateTimeFormatter.ofPattern("HH:mm"))
                + " " + theatre + " " + auditorium;
    }

    public Movie(String n, LocalDate d, LocalTime st, LocalTime ed, String a, String t) {
        name = n;
        date = d;
        startTime = st;
        endTime = ed;
        auditorium = a;
        theatre = t;
    }
}
