package vk9.vk9t4t5;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

/**
 * Ilari Sahi
 * Opiskelijanumero: 0438594
 * 21.11.2016
 */
public class Movies extends ParseData {
    private ArrayList<Movie> movies = new ArrayList();

    public Movies(String content) {
        getData(content);
    }

    public ArrayList<Movie> getList() {
        return movies;
    }

    protected void parseData() {
        NodeList nodes = doc.getElementsByTagName("Show");

        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Element e = (Element)node;

            String startdate = getValue("dttmShowStart", e).replaceAll("T", " ");
            String enddate = getValue("dttmShowEnd", e).replaceAll("T", " ");

            LocalDateTime datetimeStart = LocalDateTime.parse(startdate, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
            LocalDateTime datetimeEnd = LocalDateTime.parse(enddate, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));

            LocalDate d = LocalDate.from(datetimeStart);
            LocalTime st = LocalTime.from(datetimeStart);
            LocalTime et = LocalTime.from(datetimeEnd);

            movies.add(new Movie(getValue("Title", e), d, st, et, getValue("TheatreAuditorium", e), getValue("Theatre", e)));
        }
    }
}
