package vk9.vk9t4t5;

import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;

/**
 * Ilari Sahi
 * Opiskelijanumero: 0438594
 * 21.11.2016
 */
public class Controller implements Initializable {

    public ListView list;
    public ComboBox theatreBox;
    public DatePicker datePick;
    public TextField startTime;
    public TextField endTime;
    public TextField movieName;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        String content = "";
        String line;

        try {
            URL areasURL = new URL("http://www.finnkino.fi/xml/TheatreAreas/");
            BufferedReader br = new BufferedReader(new InputStreamReader(areasURL.openStream()));

            while ((line = br.readLine()) != null) {
                content += line + "\n";
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        Theatres t = new Theatres(content);

        theatreBox.getItems().addAll(t.getList());
        theatreBox.getSelectionModel().selectFirst();
    }
    public void listMoviesButtonAction(ActionEvent actionEvent) {
        list.getItems().clear();

        String content = "";
        String line;
        LocalDate date = datePick.getValue();
        String dateString;
        if (date == null) {
            date = LocalDate.now();
        }
        dateString = date.format(DateTimeFormatter.ofPattern("dd.MM.yyyy"));

        String startTimeString = startTime.getText();
        String endTimeString = endTime.getText();

        LocalTime startTimeLT;
        LocalTime endTimeLT;


        if (startTimeString.trim().isEmpty()) {
            startTimeLT = LocalTime.MIN;
        } else {
            startTimeLT = LocalTime.parse(startTimeString);
        }

        if (endTimeString.trim().isEmpty()) {
            endTimeLT = LocalTime.MAX;
        } else {
            endTimeLT = LocalTime.parse(endTimeString);
        }

        try {
            URL moviesURL = new URL("http://www.finnkino.fi/xml/Schedule/?area=" + Integer.toString(((Theatre)theatreBox.getValue()).getId()) + "&dt=" + dateString);
            BufferedReader br = new BufferedReader(new InputStreamReader(moviesURL.openStream()));

            while ((line = br.readLine()) != null) {
                content += line + "\n";
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        Movies m = new Movies(content);
        for (Movie mov : m.getList()) {
            if (mov.startTime.isAfter(startTimeLT) && mov.startTime.isBefore(endTimeLT)) {
                list.getItems().add(mov);
            }
        }
    }

    public void nameSearchButtonAction(ActionEvent actionEvent) {
        list.getItems().clear();

        String content = "";
        String line;

        try {
            URL moviesURL = new URL("http://www.finnkino.fi/xml/Schedule/");
            BufferedReader br = new BufferedReader(new InputStreamReader(moviesURL.openStream()));

            while ((line = br.readLine()) != null) {
                content += line + "\n";
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        Movies m = new Movies(content);
        String movieNameString = movieName.getText();

        for (Movie mov : m.getList()) {
            if (mov.name.equals(movieNameString)) {
                list.getItems().add(mov);
            }
        }
    }
}
