package vk9.vk9t4t5;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;

/**
 * Ilari Sahi
 * Opiskelijanumero: 0438594
 * 21.11.2016
 */
public class Theatres extends ParseData {
    private ArrayList<Theatre> theatres = new ArrayList();

    public Theatres(String content) {
        getData(content);
    }

    public ArrayList<Theatre> getList() {
        return theatres;
    }

    protected void parseData() {
        NodeList nodes = doc.getElementsByTagName("TheatreArea");

        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Element e = (Element)node;

            theatres.add(new Theatre(getValue("Name", e), Integer.parseInt(getValue("ID", e))));
        }
    }
}
