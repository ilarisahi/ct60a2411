package vk9.vk9t4t5;

/**
 * Ilari Sahi
 * Opiskelijanumero: 0438594
 * 21.11.2016
 */
public class Theatre {
    private String name;
    private int id;

    @Override
    public String toString() {
        return name;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public Theatre(String n, int i) {
        name = n;
        id = i;
    }
}
