package vk9.vk9t3;

/**
 * Ilari Sahi
 * Opiskelijanumero: 0438594
 * 21.11.2016
 */
public class Movie {
    String name;
    String date;
    String startTime;
    String endTime;
    String auditorium;

    @Override
    public String toString() {
        return name + " " + startTime + " " + auditorium;
    }

    public Movie(String n, String d, String st, String ed, String a) {
        name = n;
        date = d;
        startTime = st;
        endTime = ed;
        auditorium = a;
    }
}
