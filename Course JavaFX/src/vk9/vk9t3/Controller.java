package vk9.vk9t3;

import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;

/**
 * Ilari Sahi
 * Opiskelijanumero: 0438594
 * 21.11.2016
 */
public class Controller implements Initializable {

    public ListView list;
    public ComboBox theatreBox;
    public DatePicker datePick;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        String content = "";
        String line;

        try {
            URL areasURL = new URL("http://www.finnkino.fi/xml/TheatreAreas/");
            BufferedReader br = new BufferedReader(new InputStreamReader(areasURL.openStream()));

            while ((line = br.readLine()) != null) {
                content += line + "\n";
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        Theatres t = new Theatres(content);

        theatreBox.getItems().addAll(t.getList());
        theatreBox.getSelectionModel().selectFirst();
    }
    public void listMoviesButtonAction(ActionEvent actionEvent) {
        list.getItems().clear();

        String content = "";
        String line;
        LocalDate date = LocalDate.now();
        String dateString;

        dateString = date.format(DateTimeFormatter.ofPattern("dd.MM.yyyy"));

        try {
            URL moviesURL = new URL("http://www.finnkino.fi/xml/Schedule/?area=" + Integer.toString(((Theatre)theatreBox.getValue()).getId()) + "&dt=" + dateString);
            BufferedReader br = new BufferedReader(new InputStreamReader(moviesURL.openStream()));

            while ((line = br.readLine()) != null) {
                content += line + "\n";
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        Movies m = new Movies(content);
        list.getItems().addAll(m.getList());
    }

    public void nameSearchButtonAction(ActionEvent actionEvent) {
    }
}
