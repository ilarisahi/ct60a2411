package vk9.vk9t3;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StringReader;

/**
 * Ilari Sahi
 * Opiskelijanumero: 0438594
 * 21.11.2016
 */
abstract public class ParseData {
    protected Document doc;

    protected void getData(String content) {
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

            doc = dBuilder.parse(new InputSource(new StringReader(content)));

            doc.getDocumentElement().normalize();

            parseData();
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }
    }

    abstract protected void parseData();

    protected String getValue(String tag, Element e) {
        return (e.getElementsByTagName(tag).item(0)).getTextContent();
    }
}
