package vk9.vk9t2;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;

/**
 * Ilari Sahi
 * Opiskelijanumero: 0438594
 * 21.11.2016
 */
public class Theatres {
    private Document doc;
    private ArrayList<Theatre> theatres = new ArrayList();

    public Theatres(String content) {
        getTheaters(content);
    }

    public ArrayList<Theatre> getList() {
        return theatres;
    }

    private void getTheaters(String content) {
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

            doc = dBuilder.parse(new InputSource(new StringReader(content)));

            doc.getDocumentElement().normalize();

            parseData();
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }
    }

    private void parseData() {
        NodeList nodes = doc.getElementsByTagName("TheatreArea");

        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Element e = (Element)node;

            theatres.add(new Theatre(getValue("Name", e), Integer.parseInt(getValue("ID", e))));
        }
    }

    private String getValue(String tag, Element e) {
        return (e.getElementsByTagName(tag).item(0)).getTextContent();
    }
}
