package vk11.vk11t1t2t3t4;

import java.util.ArrayList;

/**
 * Ilari Sahi
 * Opiskelijanumero: 0438594
 * 9.12.2016
 */
public class ShapeHandler {
    static private ShapeHandler sh = null;
    private ArrayList shapeList = new ArrayList();

    private ShapeHandler() {

    }

    static public ShapeHandler getInstance() {
        if (sh == null) {
            sh = new ShapeHandler();
        }
        return sh;
    }

    public void addShape(Point p) {
        shapeList.add(p);
    }
}
