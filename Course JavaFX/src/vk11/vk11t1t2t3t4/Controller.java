package vk11.vk11t1t2t3t4;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;

/**
 * Ilari Sahi
 * Opiskelijanumero: 0438594
 * 9.12.2016
 */
public class Controller {

    double spX, spY, epX, epY;
    Line ln;
    public AnchorPane anchor;
    public Button addButton;
    public GridPane gridPane;
    public Pane mapPane;
    public Button lineButton;
    private ShapeHandler sh = ShapeHandler.getInstance();

    public void anchorClicked(MouseEvent mouseEvent) {
    }

    public void addAction(ActionEvent actionEvent) {
        addButton.setDisable(true);
        addButton.setText("Click on the map");
    }

    public void mapClicked(MouseEvent mouseEvent) {
        if (addButton.isDisable()) {
            Point m = new Point(mouseEvent.getSceneX(), mouseEvent.getSceneY());
            sh.addShape(m);
            mapPane.getChildren().add(m.p);

            m.p.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    System.out.println("Hei, olen " + m.name + "!");
                    getLineCoordinates((Circle)event.getSource());
                }
            });

            addButton.setText("Add a marker");
            addButton.setDisable(false);
        }
    }

    public void drawLine(ActionEvent actionEvent) {
        mapPane.getChildren().remove(ln);
        lineButton.setDisable(true);
        lineButton.setText("Choose starting point");
    }

    void getLineCoordinates(Circle c) {
        if (lineButton.getText() == "Choose starting point") {
            spX = c.getCenterX();
            spY = c.getCenterY();

            lineButton.setText("Choose ending point");
        } else if (lineButton.getText() == "Choose ending point") {
            epX = c.getCenterX();
            epY = c.getCenterY();

            ln = new Line(spX, spY, epX, epY);
            ln.setStrokeWidth(3);
            ln.setStroke(Color.web("blue"));
            mapPane.getChildren().add(ln);

            lineButton.setText("Draw a line");
            lineButton.setDisable(false);
        }
    }
}
