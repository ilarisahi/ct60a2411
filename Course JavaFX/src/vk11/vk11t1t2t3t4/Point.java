package vk11.vk11t1t2t3t4;

import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

/**
 * Ilari Sahi
 * Opiskelijanumero: 0438594
 * 9.12.2016
 */
public class Point {
    Circle p;
    String name;

    public Point(double x, double y) {
        p = new Circle(5, Color.web("pink"));
        p.setCenterY(y);
        p.setCenterX(x);
        p.setStroke(Color.web("black"));
        name = "piste";
    }
}
