package vk11.vk11t5;


import javafx.scene.shape.Line;

import java.util.ArrayList;

/**
 * Ilari Sahi
 * Opiskelijanumero: 0438594
 * 9.12.2016
 */
public class ShapeHandler {
    static private ShapeHandler sh = null;
    private ArrayList shapeList = new ArrayList();

    private ShapeHandler() {

    }

    static public ShapeHandler getInstance() {
        if (sh == null) {
            sh = new ShapeHandler();
        }
        return sh;
    }

    public void addShape(Point p) {
        shapeList.add(p);
    }

    public void addShape(Line l) {
        shapeList.add(l);
    }
}
