package vk10.vk10t3;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Ilari Sahi
 * Opiskelijanumero: 0438594
 * 22.11.2016
 */
public class Controller implements Initializable {
    public WebView web;
    public TextField address;
    public AnchorPane anchor;
    public TextField script;
    private Stage stage;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        address.setText("google.com");
        web.getEngine().load("http://google.com");



        // http://www.java2s.com/Code/Java/JavaFX/WebEngineLoadListener.htm
        web.getEngine().getLoadWorker().stateProperty().addListener(new ChangeListener<Worker.State>() {
            @Override
            public void changed(ObservableValue<? extends Worker.State> observable, Worker.State oldValue, Worker.State newValue) {
                if (newValue == Worker.State.SUCCEEDED) {
                    address.setText(web.getEngine().getLocation());
                    stage = (Stage)anchor.getScene().getWindow();
                    stage.setTitle(web.getEngine().getTitle());
                }
            }
        });
    }

    public void addressBarAction(ActionEvent actionEvent) {
        String tmpURL = address.getText();

        if (tmpURL.equals("index.html")) {
            web.getEngine().load(getClass().getResource("index.html").toExternalForm());
        } else {
            if (!(tmpURL.startsWith("http://"))) {
                tmpURL = "http://" + tmpURL;
            } else {
                address.setText(tmpURL.replaceFirst("http://", ""));
            }
            web.getEngine().load(tmpURL);
        }
    }

    public void reloadAction(ActionEvent actionEvent) {
        web.getEngine().reload();
    }

    public void goAction(ActionEvent actionEvent) {
        addressBarAction(actionEvent);
    }

    public void scriptBarAction(ActionEvent actionEvent) {
        web.getEngine().executeScript(script.getText());
        script.setText("");
    }

    public void executeButtonAction(ActionEvent actionEvent) {
        scriptBarAction(actionEvent);
    }

    public void initializeAction(ActionEvent actionEvent) {
        web.getEngine().executeScript("initialize()");
    }

    public void shoutOutAction(ActionEvent actionEvent) {
        web.getEngine().executeScript("document.shoutOut()");
    }
}
