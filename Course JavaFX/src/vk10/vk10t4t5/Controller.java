package vk10.vk10t4t5;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ArrayList;
import java.util.ListIterator;
import java.util.ResourceBundle;

/**
 * Ilari Sahi
 * Opiskelijanumero: 0438594
 * 22.11.2016
 */
public class Controller implements Initializable {
    public WebView web;
    public TextField address;
    public AnchorPane anchor;
    public TextField script;
    private Stage stage;
    private ArrayList<String> history = new ArrayList<>();
    private ListIterator<String> lit;
    private boolean nextCalled = true;
    private boolean prevCalled = false;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        address.setText("http://google.com");
        web.getEngine().load("http://google.com");
        history.add("http://google.com");
        lit = history.listIterator(history.size());

        // http://www.java2s.com/Code/Java/JavaFX/WebEngineLoadListener.htm
        /*web.getEngine().getLoadWorker().stateProperty().addListener(new ChangeListener<Worker.State>() {
            @Override
            public void changed(ObservableValue<? extends Worker.State> observable, Worker.State oldValue, Worker.State newValue) {
                if (newValue == Worker.State.SUCCEEDED) {
                    if (history.size() < 10) {
                        history.add(web.getEngine().getLocation());
                    } else {
                        history.remove(0);
                        history.add(web.getEngine().getLocation());
                    }

                    address.setText(web.getEngine().getLocation());
                    stage = (Stage)anchor.getScene().getWindow();
                    stage.setTitle(web.getEngine().getTitle());
                }
            }
        });*/
    }

    public void addressBarAction(ActionEvent actionEvent) {
        String tmpURL = address.getText();

        if (tmpURL.equals("index.html")) {
            web.getEngine().load(getClass().getResource("index.html").toExternalForm());
        } else {
            if (!(tmpURL.startsWith("http://"))) {
                tmpURL = "http://" + tmpURL;
                address.setText(tmpURL);
            }
            web.getEngine().load(tmpURL);
        }

        if (history.size() < 10) {
            history.add(address.getText());
        } else {
            history.remove(0);
            history.add(address.getText());
        }

        lit = history.listIterator(history.size());
        nextCalled = true;
        prevCalled = false;

        System.out.println("iterator: " + lit.nextIndex());
        for (String s : history) {
            System.out.println(s);
        }
    }

    public void reloadAction(ActionEvent actionEvent) {
        web.getEngine().reload();
    }

    public void goAction(ActionEvent actionEvent) {
        addressBarAction(actionEvent);
    }

    public void scriptBarAction(ActionEvent actionEvent) {
        web.getEngine().executeScript(script.getText());
        script.setText("");
    }

    public void executeButtonAction(ActionEvent actionEvent) {
        scriptBarAction(actionEvent);
    }

    public void initializeAction(ActionEvent actionEvent) {
        web.getEngine().executeScript("initialize()");
    }

    public void shoutOutAction(ActionEvent actionEvent) {
        web.getEngine().executeScript("document.shoutOut()");
    }

    public void previousAction(ActionEvent actionEvent) {
        System.out.println("prev i: " + lit.previousIndex());
        if (nextCalled && (lit.previousIndex() > -1)) {
            lit.previous();
        }

        if (lit.hasPrevious() && (lit.previousIndex() > -1)) {
            String prevString = lit.previous();
            System.out.println(prevString);
            web.getEngine().load(prevString);
            address.setText(prevString);

            prevCalled = true;
            nextCalled = false;
        }
    }

    public void nextAction(ActionEvent actionEvent) {
        System.out.println("next i: " + lit.nextIndex());
        if (prevCalled) {
            lit.next();
        }

        if (lit.hasNext() && (lit.nextIndex() < (history.size()))) {
            String nextString = lit.next();
            web.getEngine().load(nextString);
            System.out.println(nextString);
            address.setText(nextString);

            nextCalled = true;
            prevCalled = false;
        }
    }
}
