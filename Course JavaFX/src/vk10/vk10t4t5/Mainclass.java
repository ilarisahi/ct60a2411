package vk10.vk10t4t5;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Ilari Sahi
 * Opiskelijanumero: 0438594
 * 22.11.2016
 */
public class Mainclass extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("webbrowser.fxml"));
        primaryStage.setTitle("Web Browser");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
