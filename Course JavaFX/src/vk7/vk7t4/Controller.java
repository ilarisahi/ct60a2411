package vk7.vk7t4;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.KeyCode;

/**
 * Ilari Sahi
 * Opiskelijanumero: 0438594
 * 19.11.2016
 */
public class Controller {


    public Label label;
    public TextField textField;

    @FXML
    public void textFieldTypedAction(KeyEvent keyEvent) {
        if (keyEvent.getCode().equals(KeyCode.ENTER)) {
            textField.setText("");
        } else {
            label.setText(textField.getText());
        }
    }
}
