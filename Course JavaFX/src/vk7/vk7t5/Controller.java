package vk7.vk7t5;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.*;

/**
 * Ilari Sahi
 * Opiskelijanumero: 0438594
 * 19.11.2016
 */
public class Controller {
    public TextArea textArea;
    public TextField fileNameField;
    public Button loadButton;
    public Button saveButton;
    private String filename = "";
    private Stage secondary;
    private String line;
    private String tmp;

    public void popUp(Stage secondaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("popup.fxml"));
        secondaryStage.setTitle("Anna tiedostonimi");
        secondaryStage.setScene(new Scene(root));
        secondaryStage.initModality(Modality.APPLICATION_MODAL);
        secondaryStage.showAndWait();
        if (secondaryStage.getUserData() != null) {
            filename = secondaryStage.getUserData().toString();
        }
    }

    @FXML
    void loadButtonAction(ActionEvent actionEvent) throws Exception{
        tmp = "";
        secondary = new Stage();
        popUp(secondary);

        if (filename != null && filename != "") {
            try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
                while ((line = br.readLine()) != null) {
                    tmp += line + "\n";
                }
                br.close();
            } catch (IOException e) {
                System.err.println("Caught IOException: " + e.getMessage());
            }
        }

        textArea.setText(tmp);
    }

    @FXML
    void saveButtonAction(ActionEvent actionEvent) throws Exception {
        secondary = new Stage();
        popUp(secondary);

        if (filename != null && filename != "") {
            try (BufferedWriter bw = new BufferedWriter(new FileWriter(filename))) {
                bw.write(textArea.getText());
                bw.close();
                textArea.setText("");
            } catch (IOException e) {
                System.err.println("Caught IOException: " + e.getMessage());
            }
        }
    }

    @FXML
    void fileNameSubmitButtonAction(ActionEvent actionEvent) {
        filename = fileNameField.getText();
        secondary = (Stage)fileNameField.getScene().getWindow();
        secondary.setUserData(filename);
        secondary.close();
    }
}
