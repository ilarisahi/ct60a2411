package vk7.vk7t3;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 * Ilari Sahi
 * Opiskelijanumero: 0438594
 * 19.11.2016
 */
public class Controller {


    public Label label;
    public Button button;
    public TextField textField;

    @FXML
    void buttonPressAction() {
        label.setText(textField.getText());
        textField.setText("");
    }
}
