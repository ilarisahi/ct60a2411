
package vk8.vk8t5;

import java.math.BigDecimal;
import java.util.ArrayList;

/**
 * Ilari Sahi
 * Opiskelijanumero: 0438594
 * 20.11.2016
 */
public class BottleDispenser {

    private ArrayList<Bottle> bottle_array = new ArrayList();
    private BigDecimal money = new BigDecimal(0);
    static private BottleDispenser bt = null;
    private Bottle lastPurchase;

    public void populateDispenser() {
        bottle_array.add(new Bottle("Pepsi Max", "Pepsi", 0.33, 1.3));
        bottle_array.add(new Bottle("Pepsi Max", "Pepsi", 0.5, 1.8));
        bottle_array.add(new Bottle("Pepsi Max", "Pepsi", 1.5, 2.2));
        bottle_array.add(new Bottle("Coca-Cola Zero", "Coca-Cola", 0.33, 1.8));
        bottle_array.add(new Bottle("Coca-Cola Zero", "Coca-Cola", 0.5, 2.0));
        bottle_array.add(new Bottle("Coca-Cola Zero", "Coca-Cola", 1.5, 2.5));
        bottle_array.add(new Bottle("Fanta Zero", "Fanta", 0.33, 1.30));
        bottle_array.add(new Bottle("Fanta Zero", "Fanta", 0.5, 1.95));
        bottle_array.add(new Bottle("Fanta Zero", "Fanta", 0.5, 1.95));
    }

    private BottleDispenser() {
        populateDispenser();
    }

    public Bottle getBottle(int i) {
        return bottle_array.get(i-1);
    }

    static public BottleDispenser getInstance() {
        if (bt == null) {
            bt = new BottleDispenser();
        }
        return bt;
    }

    private void deleteBottle(int i) {
        bottle_array.remove(i-1);
    }

    public void addMoney(BigDecimal x) {
        money = money.add(x);
        System.out.println(money);
    }

    public String buyBottle(String n, double v) {
        String tmp;
        if(bottle_array.size() != 0) {
            for (Bottle b : bottle_array) {
                if ((b.name() == n) && (b.volume() == v)) {
                    if (money.doubleValue() > b.price()) {
                        money = money.subtract(BigDecimal.valueOf(b.price()));
                        tmp = "KACHUNK! You just bought a " + b.name() + "!";
                        lastPurchase = b;
                        bottle_array.remove(b);
                        return tmp;
                    } else {
                        tmp = "Not enough money!";
                        return tmp;
                    }
                }
            }
            tmp = "No such product available, sorry!";
        } else {
            tmp = "No bottles left... Sorry!";
        }
        return tmp;
    }

    public String returnMoney() {
        String tmp;
        tmp = "Cling cling. There goes money. You got " + money + " €";
        money = new BigDecimal(0);
        return tmp;
    }

    public Bottle getLastPurchase() {
        return lastPurchase;
    }

    public void printList() {
        int i = 1;
        for(Bottle temp_bottle : bottle_array) {
            System.out.println(i + ". Nimi: " + temp_bottle.name());
            System.out.println("\tKoko: " + temp_bottle.volume() + "\tHinta: " + temp_bottle.price());
            i++;
        }
    }
}
