
package vk8.vk8t1;

import java.util.ArrayList;

/**
 * Ilari Sahi
 * Opiskelijanumero: 0438594
 * 20.11.2016
 */
public class BottleDispenser {
    
    private int bottles;
    private ArrayList<Bottle> bottle_array = new ArrayList();
    private double money;
    static private BottleDispenser bt = null;
    
    private BottleDispenser() {
        bottles = 6;
        money = 0;
        
        bottle_array.add(new Bottle("Pepsi Max", "Pepsi", 0.5, 1.8));
        bottle_array.add(new Bottle("Pepsi Max", "Pepsi", 1.5, 2.2));
        bottle_array.add(new Bottle("Coca-Cola Zero", "Coca-Cola", 0.5, 2.0));
        bottle_array.add(new Bottle("Coca-Cola Zero", "Coca-Cola", 1.5, 2.5));
        bottle_array.add(new Bottle("Fanta Zero", "Fanta", 0.5, 1.95));
        bottle_array.add(new Bottle("Fanta Zero", "Fanta", 0.5, 1.95));
    }

    static public BottleDispenser getInstance() {
        if (bt == null) {
            bt = new BottleDispenser();
        }
        return bt;
    }
    
    private void deleteBottle(int i) {
        bottle_array.remove(i-1);
    }
    
    public void addMoney() {
        money += 1;
        System.out.println("Klink! Lisää rahaa laitteeseen!");
    }
    
    public void buyBottle(int i) {        
        if(bottles > 0) { 
            Bottle temp_bottle = bottle_array.get(i-1);
            if(money > temp_bottle.price()) {
                bottles -= 1;
                money -= temp_bottle.price();
                System.out.println("KACHUNK! " + temp_bottle.name() + " tipahti masiinasta!");
                deleteBottle(i);
            } else{
                System.out.println("Syötä rahaa ensin!");
            }
        } else if(bottles == 0){
            System.out.println("Error! Ei pulloja jäljellä!");
        }
    }
    
    public void returnMoney() {
        System.out.printf("Klink klink. Sinne menivät rahat! Rahaa tuli ulos %.2f€\n", money);
        money = 0;
    }
    
    public void printList() {
        int i = 1;
        for(Bottle temp_bottle : bottle_array) {
            System.out.println(i + ". Nimi: " + temp_bottle.name());
            System.out.println("\tKoko: " + temp_bottle.volume() + "\tHinta: " + temp_bottle.price());
            i++;
        }
    }
}
