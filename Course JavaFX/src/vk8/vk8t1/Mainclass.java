package vk8.vk8t1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Ilari Sahi
 * Opiskelijanumero: 0438594
 * 20.11.2016
 */
public class Mainclass {
    public static void main(String args[]){
        
        int selection = 0;
        String tmp = null;
        BottleDispenser dispenser = BottleDispenser.getInstance();
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        
        do {
            System.out.println();
            System.out.println("*** LIMSA-AUTOMAATTI ***");
            System.out.println("1) Lisää rahaa koneeseen");
            System.out.println("2) Osta pullo");
            System.out.println("3) Ota rahat ulos");
            System.out.println("4) Listaa koneessa olevat pullot");
            System.out.println("0) Lopeta");
            
            System.out.print("Valintasi: ");
            
            try {
                tmp = br.readLine();
            } catch (IOException ex) {
                System.out.println("Virhe!");
            }
            
            selection = Integer.parseInt(tmp);
            
            switch(selection) {
                case 1:
                    dispenser.addMoney();
                    break;
                case 2:
                    dispenser.printList();
                    
                    System.out.print("Valintasi: ");            
                    try {
                        tmp = br.readLine();
                    } catch (IOException ex) {
                        System.out.println("Virhe!");
                    }
                    selection = Integer.parseInt(tmp);
                    
                    dispenser.buyBottle(selection);
                    break;
                case 3:
                    dispenser.returnMoney();
                    break;
                case 4:
                    dispenser.printList();
                    break;
                case 0:
                    break;
                default:
                    System.out.println("Virheellinen syöte!");
                    break;
            }            
        } while(selection != 0);
    }
}
