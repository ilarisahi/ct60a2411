package vk8.vk8t3;

import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseEvent;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URL;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.ResourceBundle;

/**
 * Ilari Sahi
 * Opiskelijanumero: 0438594
 * 20.11.2016
 */
public class Controller implements Initializable {

    public TextArea log;
    public Slider moneySlider;
    public Label moneyLabel;
    private BottleDispenser bd;
    private Date date;
    private BigDecimal money = new BigDecimal(0);

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        bd = BottleDispenser.getInstance();
    }

    public void insertButtonAction(ActionEvent actionEvent) {
        date = new GregorianCalendar().getTime();
        money = new BigDecimal(moneySlider.getValue()).setScale(2, RoundingMode.HALF_DOWN);
        bd.addMoney(money);
        log.setText(String.valueOf(date) + " - Added " + money + " € to machine");
        moneySlider.setValue(0.00);
        moneyLabel.setText("0.00 €");
    }

    public void cashOutButtonAction(ActionEvent actionEvent) {
        date = new GregorianCalendar().getTime();
        log.setText(String.valueOf(date) + " - " + bd.returnMoney());
    }

    public void buyButtonAction(ActionEvent actionEvent) {
        date = new GregorianCalendar().getTime();
        log.setText(String.valueOf(date) + " - " + bd.buyBottle(1));
    }

    public void moneySliderAction(MouseEvent mouseEvent) {
        moneyLabel.setText(String.format("%.2f €", moneySlider.getValue()));
    }
}
