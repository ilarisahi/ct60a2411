package vk8.vk8t2;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;

import java.net.URL;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.ResourceBundle;

/**
 * Ilari Sahi
 * Opiskelijanumero: 0438594
 * 20.11.2016
 */
public class Controller implements Initializable {

    public TextArea log;
    private BottleDispenser bd;
    private Date date;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        bd = BottleDispenser.getInstance();
    }

    public void insertButtonAction(ActionEvent actionEvent) {
        date = new GregorianCalendar().getTime();
        bd.addMoney();
        log.setText(String.valueOf(date) + " - Added a coin");
    }

    public void cashOutButtonAction(ActionEvent actionEvent) {
        date = new GregorianCalendar().getTime();
        log.setText(String.valueOf(date) + " - " + bd.returnMoney());
    }

    public void buyButtonAction(ActionEvent actionEvent) {
        date = new GregorianCalendar().getTime();
        log.setText(String.valueOf(date) + " - " + bd.buyBottle(1));
    }

}
