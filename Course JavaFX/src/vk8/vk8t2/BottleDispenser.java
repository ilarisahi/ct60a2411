
package vk8.vk8t2;

import java.util.ArrayList;

/**
 * Ilari Sahi
 * Opiskelijanumero: 0438594
 * 20.11.2016
 */
public class BottleDispenser {

    private ArrayList<Bottle> bottle_array = new ArrayList();
    private double money;
    static private BottleDispenser bt = null;
    
    private BottleDispenser() {
        money = 0;
        
        bottle_array.add(new Bottle("Pepsi Max", "Pepsi", 0.5, 1.8));
        bottle_array.add(new Bottle("Pepsi Max", "Pepsi", 1.5, 2.2));
        bottle_array.add(new Bottle("Coca-Cola Zero", "Coca-Cola", 0.5, 2.0));
        bottle_array.add(new Bottle("Coca-Cola Zero", "Coca-Cola", 1.5, 2.5));
        bottle_array.add(new Bottle("Fanta Zero", "Fanta", 0.5, 1.95));
        bottle_array.add(new Bottle("Fanta Zero", "Fanta", 0.5, 1.95));
    }

    public Bottle getBottle(int i) {
        return bottle_array.get(i-1);
    }

    static public BottleDispenser getInstance() {
        if (bt == null) {
            bt = new BottleDispenser();
        }
        return bt;
    }
    
    private void deleteBottle(int i) {
        bottle_array.remove(i-1);
    }
    
    public void addMoney() {
        money += 1;
    }
    
    public String buyBottle(int i) {
        String tmp;
        tmp = "";
        if(bottle_array.size() != 0) {
            Bottle temp_bottle = bottle_array.get(i-1);
            if(money > temp_bottle.price()) {
                money -= temp_bottle.price();
                tmp = "KACHUNK! You just bought a " + temp_bottle.name() + "!";
                deleteBottle(i);
            } else {
                tmp = "Not enough money!";
            }
        } else {
            tmp = "No bottles left... Sorry!";
        }
        return tmp;
    }
    
    public String returnMoney() {
        String tmp;
        tmp = "Cling cling. There goes money. You got " + String.format("%.2f€", money);
        money = 0;
        return tmp;
    }
    
    public void printList() {
        int i = 1;
        for(Bottle temp_bottle : bottle_array) {
            System.out.println(i + ". Nimi: " + temp_bottle.name());
            System.out.println("\tKoko: " + temp_bottle.volume() + "\tHinta: " + temp_bottle.price());
            i++;
        }
    }
}
